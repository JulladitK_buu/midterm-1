/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.midterm1;

/**
 *
 * @author acer
 */
public class TestCoil {

    public static void main(String[] args) {
        Alien alien = new Alien("Type : Alien", "Ohm : 0.11");
        System.out.println(alien.Type + "\n" + alien.Ohm);
        System.out.println("----------------");
        ClaptonCoil clapton = new ClaptonCoil("Type : Clapton", "Ohme : 0.18");
        System.out.println(clapton.Type + "\n" + clapton.Ohm);
        System.out.println("----------------");
    }
}
